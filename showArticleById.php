<?php
include("config/init.php");
require_once("model/article.php");
require_once("model/user.php");
require_once("model/comment.php");

if (isset($_SESSION['username'])) {
    header("Location: index.php");
}

session_start();



$article = Article::find_by_id($_GET['id']);
if ($article)
    $user = User::find_by_id($article->userId);


if (isset($_POST["submit"])) {
    $comment = new Comment();
    $comment->description = $_POST["description"];
    $comment->commentDate =  date("Y-m-d H:i:s");
    $comment->articleId = $article->id;
    $comment->userId = $_SESSION['userId'];
    if ($comment->create()) {
        header("showArticleById.php?id=" . $article->id);
        unset($_POST);
    } else {
        echo "<script>alert('Wow!.')</script>";
    }
}

$comments = Comment::getCommentsByArticleId($article->id);
?>


<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="assets/img/favicon.ico">
    <title>Mediumish - A Medium style template by WowThemes.net</title>
    <!-- Bootstrap core CSS -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet">
    <!-- Fonts -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Righteous%7CMerriweather:300,300i,400,400i,700,700i" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="assets/css/mediumish.css" rel="stylesheet">
</head>

<body>

    <!-- Begin Nav
================================================== -->
    <?php
    include("views/navbar.php");

    ?>
    <!-- End Nav
================================================== -->

    <!-- Begin Article
================================================== -->
    <div class="container">
        <div class="row">

            <!-- Begin Post -->
            <div class="col-md-6 col-md-offset-2 col-xs-3">
                <div class="mainheading">

                    <!-- Begin Top Meta -->
                    <div class="row post-top-meta">
                        <div class="col-md-2">
                            <a href="author.html"><img class="author-thumb" src="https://www.gravatar.com/avatar/e56154546cf4be74e393c62d1ae9f9d4?s=250&amp;d=mm&amp;r=x" alt="Sal"></a>
                        </div>
                        <div class="col-md-10">
                            <a class="link-dark" href="author.html"> <?php echo $user->username ?></a><a href="#" class="btn follow">Follow</a>
                        </div>
                    </div>
                    <!-- End Top Menta -->

                    <h1 class="posttitle"><?php echo $article->title ?></h1>

                </div>

                <!-- Begin Featured Image -->
                <img class="featured-image img-fluid" src=<?php echo "images/" . $article->filename ?> alt="">
                <!-- End Featured Image -->

                <!-- Begin Post Content -->
                <div class="article-post">
                    <p> <?php echo $article->description ?> </p>
                </div>
                <!-- End Post Content -->
            </div>
            <!-- Begin Post -->
            <div class="col-md-6 col-md-offset-2 col-xs-3">
                <div class="mainheading">
                    <form action="<?php echo "showArticleById.php?id=" . $article->id ?>" method="post">
                        <div class="form-group">
                            <label for="description"><?php echo $_SESSION['username'] . ":" ?></label>
                            <textarea class="form-control" name="description" rows="3" placeholder="Add a comment"></textarea>
                        </div>
                        <div class="col-12">
                            <input type="submit" name="submit">
                        </div>
                    </form>
                </div>
                <ul class="list-group list-group-flush">
                    <?php if ($comments) {
                        foreach ($comments as $comment) : ?>


                            <?php $user = User::find_by_id($comment->userId) ?>
                            <li class="list-group-item"><span style="font-weight: bold;"><?php echo $user->username . ": " ?></span> <?php echo $comment->description ?>
                                <form action='deleteComment.php?id="<?php echo $comment->id; ?>"' method="post">
                                    <?php
                                    if ($comment->userId == $_SESSION["userId"] || $article->userId == $_SESSION["userId"]) { ?>
                                        <input aria-label='delete item' type="submit" name="submit" value="x">
                                    <?php } ?>

                                </form>
                            </li>

                    <?php endforeach;
                    } ?>
                </ul>
            </div>
        </div>
        <!-- Begin Footer
                
================================================== -->
        <?php
        include("views/footbar.php");

        ?>
        <!-- End Footer
================================================== -->

        <!-- Bootstrap core JavaScript
================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="assets/js/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script>
        <script src="assets/js/bootstrap.min.js"></script>
        <script src="assets/js/ie10-viewport-bug-workaround.js"></script>
        <script src="assets/js/mediumish.js"></script>
</body>

</html>