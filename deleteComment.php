<?php
require_once("model/comment.php");

$comment = Comment::find_by_id($_GET['id']);
if ($comment) {
    $comment->delete();
    header('Location:showArticleById.php?id=' . $comment->articleId);
}
