<?php
include("config/init.php");
require_once("model/article.php");
require_once("model/user.php");
require_once("model/comment.php");

$articles = Article::find_all();

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="assets/img/favicon.ico">
    <title>Mediumish - A Medium style template by WowThemes.net</title>
    <!-- Bootstrap core CSS -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet">
    <!-- Fonts -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Righteous" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="assets/css/mediumish.css" rel="stylesheet">
</head>

<body>

    <!-- Begin Nav
================================================== -->
    <?php
    include("views/navbar.php");

    ?>
    <!-- End Nav
================================================== -->

    <!-- Begin Site Title
================================================== -->
    <?php
    include("views/siteTitle.php");

    ?>
    <?php


    if (!isset($_SESSION['username'])) {
        header("Location: index.php");
    }

    ?>
    <!-- End Site Title
================================================== -->


    <!-- Begin List Posts
	================================================== -->
    <section class="recent-posts">
        <div class="section-title">
            <h2><span>All Articles</span></h2>
        </div>

        <div class="card-columns listrecent">
            <?php foreach ($articles as $article) : ?>
                <?php
                $user = User::find_by_id($article->userId);
                $numComm = Comment::getCommentsByArticleId($article->id);
                ?>
                <!-- begin post -->
                <div class="card">
                    <a <?php echo "href = showArticleById.php?id=" . $article->id ?>>
                        <img class="img-fluid" src=<?php echo "images/" . $article->filename ?> alt="">
                    </a>
                    <div class="card-block">
                        <h2 class="card-title"><?php echo $article->title ?></h2>
                        <h4 class="card-text"><?php echo $article->description ?>.</h4>
                        <div class="metafooter">
                            <div class="wrapfooter">
                                <span class="meta-footer-thumb">
                                    <a href="author.html"><img class="author-thumb" src="https://www.gravatar.com/avatar/e56154546cf4be74e393c62d1ae9f9d4?s=250&amp;d=mm&amp;r=x" alt="Sal"></a>
                                </span>
                                <span class="author-meta">
                                    <span class="post-name"><a href="author.html"><?php echo $user->username ?></a></span><br />
                                    <span class="post-date"><?php echo $article->dateUpload ?></span><span class="post-read"></span>
                                </span>
                                <span class="post-read-more"><a <?php echo "href = showArticleById.php?id=" . $article->id ?> title=" Read Story"><svg class="svgIcon-use" width="25" height="25" viewbox="0 0 25 25">
                                            <path d="M19 6c0-1.1-.9-2-2-2H8c-1.1 0-2 .9-2 2v14.66h.012c.01.103.045.204.12.285a.5.5 0 0 0 .706.03L12.5 16.85l5.662 4.126a.508.508 0 0 0 .708-.03.5.5 0 0 0 .118-.285H19V6zm-6.838 9.97L7 19.636V6c0-.55.45-1 1-1h9c.55 0 1 .45 1 1v13.637l-5.162-3.668a.49.49 0 0 0-.676 0z" fill-rule="evenodd"></path>
                                        </svg></a></span>
                                <?php
                                if ($numComm) {

                                ?>
                                    <span> we have <?php echo count($numComm) ?> comments</span>
                                <?php
                                } else {
                                    echo     "<span> we have no comments</span>";
                                }
                                ?>
                                <?php if ($article->userId == $_SESSION["userId"]) { ?>
                                    <samp><a <?php echo "href = updatePost.php?id=" . $article->id ?>>update</a></samp>
                                <?php } ?>

                            </div>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>

            <!-- end post -->
        </div>

    </section>
    <!-- End List Posts
	================================================== -->

    <!-- Begin Footer
	================================================== -->
    <?php
    include("views/footbar.php");

    ?>
    <!-- End Footer
	================================================== -->

    </div>
    <!-- /.container -->

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="assets/js/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/js/ie10-viewport-bug-workaround.js"></script>
</body>

</html>