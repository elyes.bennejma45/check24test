<?php

class Database
{
    private $server = "localhost";
    private $user = "root";
    private $pass = "";
    private $db = "blog24";

    public $connection;

    function __construct()
    {

        $this->getConnection();
    }

    public function getConnection()
    {
        $conn = null;
        try {
            $this->connection = mysqli_connect($this->server, $this->user, $this->pass, $this->db);
        } catch (Exception $exception) {
            echo "Connection error: " . $exception->getMessage();
        }

        return $conn;
    }
    public function query($sql)
    {
        //$result = mysqli_query($this->connection,$sql);
        $result = $this->connection->query($sql);
        $this->confirm_query($result);
        return $result;
    }

    private function confirm_query($result)
    {
        if (!$result) {
            die("Query failed" . $this->connection->error);
        }
    }

    public function escape_string($string)
    {

        $escaped_string = $this->connection->real_escape_string($string);
        return $escaped_string;
    }

    public function the_insert_id()
    {

        return mysqli_insert_id($this->connection);
    }
}

$database = new Database();
