<?php
include("config/init.php");
require_once("model/article.php");
require_once("model/user.php");

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="assets/img/favicon.ico">
    <title>Mediumish - A Medium style template by WowThemes.net</title>
    <!-- Bootstrap core CSS -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet">
    <!-- Fonts -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Righteous%7CMerriweather:300,300i,400,400i,700,700i" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="assets/css/mediumish.css" rel="stylesheet">
</head>

<body>

    <!-- Begin Nav
================================================== -->
    <?php
    include("views/navbar.php");

    ?>
    <!-- End Nav
================================================== -->

    <!-- Begin Site Title
================================================== -->
    <?php
    include("views/siteTitle.php");
    if (!isset($_SESSION['username'])) {
        header("Location: index.php");
    }
    ?>
    <!-- End Site Title
================================================== -->

    <!-- Begin Form
================================================== -->

    <?php
    $message = "";
    $article = Article::find_by_id($_GET["id"]);

    if (isset($_POST["submit"])) {
        # code...
        $article->title = $_POST['title'];
        $article->description = $_POST['description'];
        $article->dateUpload = date("Y-m-d H:i:s");
        $article->userId = $_SESSION['userId'];
        $article->save();
        header("Location: welcome.php");

        if (!$article->save()) {
            $message = join('<br>', $article->error);
        }
    }

    ?>
    <div class="container">
        <?php
        echo $message; ?>
        <form class="row g-3" action=<?php echo "updatePost.php?id=" . $article->id ?> method="post" enctype="multipart/form-data">
            <div class="col-md-6">
                <label for="title" class="form-label">Title</label>
                <input type="text" class="form-control" name="title" value="<?php echo $article->title ?>">
            </div>
            <div class="col-md-6">
                <label for="file_upload" class="form-label">Add a file</label>
                <input class="form-control" type="file" name="file_upload">
            </div>
            <div class="col-md-12">
                <label for="description">Description</label>
                <textarea class="form-control" placeholder="Leave a Description here" name="description"><?php echo $article->description ?></textarea>
            </div>
            <div class=" col-12">
                <input type="submit" name="submit">
            </div>
        </form>
    </div>

    <!-- End Form
================================================== -->


    <!-- Begin Footer
	================================================== -->
    <?php
    include("views/footbar.php");

    ?>
    <!-- End Footer
	================================================== -->

    <!-- Bootstrap core JavaScript
================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->

    <script src="assets/js/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/js/ie10-viewport-bug-workaround.js"></script>
    <script src="assets/js/mediumish.js"></script>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
</body>

</html>