<?php
require_once('./config/global_objectDB.php');

class User extends glob_objectDB
{
    protected static $db_table = "users";
    protected static $db_table_fields = array('username', 'email', 'password');
    public $id;
    public $username;
    public $email;
    public $password;


    public static function verify_user($email, $password = null)
    {
        global $database;

        $email = $database->escape_string($email);
        $password = $database->escape_string($password);
        $sql = "SELECT * FROM " . self::$db_table . " WHERE";
        $sql .= " email = '{$email}' ";

        if ($password != null)
            $sql .= "AND password ='{$password}'";

        $sql .= " LIMIT 1";
        $res = self::findQuery($sql); //depile le premier elémen du tableau
        return !empty($res) ? array_shift($res) : null;
    }
}
