<?php
require_once('./config/global_objectDB.php');

class Comment extends glob_objectDB
{
    protected static $db_table = "comments";
    protected static $db_table_fields = array('description', 'commentDate', 'articleId', 'userId');
    public $id;
    public $description;
    public $commentDate;
    public $articleId;
    public $userId;


    public static function getCommentsByArticleId($idArticle)
    {
        global $database;

        $idArticle = $database->escape_string($idArticle);
        $sql = "SELECT * FROM " . self::$db_table . " WHERE";
        $sql .= " articleId = '{$idArticle}' ";

        $res = self::findQuery($sql); //depile le premier elémen du tableau
        return !empty($res) ? $res : null;
    }
}
